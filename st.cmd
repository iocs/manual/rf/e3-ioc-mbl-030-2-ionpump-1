require gammaspc
require essioc

epicsEnvSet("ENGINEER", "Gabriel Fedel <gabriel.fedel@ess.eu>")

iocshLoad("$(essioc_DIR)/common_config.iocsh")

epicsEnvSet("P", "MBL-030RFC:")
epicsEnvSet("R", "RFS-VacPS-210:")
epicsEnvSet("PORT", "GAMMA")
epicsEnvSet("IP", "mbl3-rf2-ip1.tn.esss.lu.se:23")

iocshLoad $(gammaspc_DIR)/gammaSpce.iocsh
